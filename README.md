# tsc-dkms

CentOS RPM for [tsc](https://github.com/icshwi/tsc) kernel modules.

This RPM includes:

- the source for the kernel modules managed by dkms (tsc and pon)
- udev rules required

To build a new version of the RPM:

- update the version in the spec file
- push your changes to check that the RPM can be built by gitlab-ci
- tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
