#!/bin/bash
set -e

# Download sources
mkdir -p SOURCES
spectool -g -C ./SOURCES SPECS/tsc-dkms.spec
# Build RPM
rpmbuild -v --define "%_topdir `pwd`" -bb SPECS/tsc-dkms.spec

tree RPMS

# Remove unwanted RPM
rm -f RPMS/x86_64/tsc-dkms-debuginfo-*.rpm
